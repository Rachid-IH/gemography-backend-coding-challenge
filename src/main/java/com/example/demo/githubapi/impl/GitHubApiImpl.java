package com.example.demo.githubapi.impl;

import java.time.LocalDate;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import com.example.demo.githubapi.GitHubApi;
import com.example.demo.githubapi.properties.Properties;
import com.example.demo.model.GithubReposData;
import com.example.demo.model.Repository;

@Service
public class GitHubApiImpl implements GitHubApi {

	@Autowired
	private RestTemplate restTemplate;

	@Override
	public List<Repository> getStarredRepos(LocalDate date, int size) {
		ResponseEntity<GithubReposData> response = 
			restTemplate.getForEntity(Properties.getGithubStarredReposUrl(date, size), GithubReposData.class);
		
		return response.getBody().getItems();
		
	}

}
