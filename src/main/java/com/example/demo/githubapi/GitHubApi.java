package com.example.demo.githubapi;

import java.time.LocalDate;
import java.util.List;

import com.example.demo.model.Repository;

public interface GitHubApi {
	
	List<Repository> getStarredRepos(LocalDate date, int size);

}
