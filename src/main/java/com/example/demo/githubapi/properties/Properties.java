package com.example.demo.githubapi.properties;

import java.text.MessageFormat;
import java.time.LocalDate;
import java.util.ResourceBundle;

import org.springframework.stereotype.Component;

@Component
public class Properties {
	
	private static ResourceBundle resourceBundle;
	
	private static final String GITHUB_STARRED_REPOS_URI;
	private static final String GITHUB_STARRED_REPOS_DISPLAY_FORMAT;
	
	static {
		resourceBundle = ResourceBundle.getBundle("github");
		
		GITHUB_STARRED_REPOS_URI = resourceBundle.getString("github.starred.repos.uri");
		GITHUB_STARRED_REPOS_DISPLAY_FORMAT = resourceBundle.getString("github.starred.repos.display.format");
	}
	
	public static String getGithubStarredReposUrl(LocalDate date, int size){
		return MessageFormat.format(GITHUB_STARRED_REPOS_URI, date.toString(), size);
	}
	
	public static String getGithubFormattedLine(String language, int reposSize, String logins){
		return MessageFormat.format(GITHUB_STARRED_REPOS_DISPLAY_FORMAT, language, reposSize, logins);
	}

}
