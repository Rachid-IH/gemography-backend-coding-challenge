package com.example.demo.model;

import java.io.Serializable;

import lombok.Getter;
import lombok.Setter;

@Getter @Setter
public class Repository implements Serializable {
	
	private static final long serialVersionUID = 1L;
	
	private long id;
	private String name;
	private String full_name;
	private Owner owner;
	private String description;
	private String language;
	
}
