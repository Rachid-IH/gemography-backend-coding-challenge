package com.example.demo.model;

import java.io.Serializable;

import lombok.Getter;
import lombok.Setter;

@Getter @Setter
public class Owner implements Serializable {
	
	private static final long serialVersionUID = 1L;
	
	private long id;
	private String login;
	private String avatar_url;
	private String url;
	private String type;

}
