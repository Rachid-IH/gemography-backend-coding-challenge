package com.example.demo;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.StandardOpenOption;
import java.time.LocalDate;
import java.util.List;
import java.util.Map;
import java.util.function.Function;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.util.ResourceUtils;
import org.springframework.util.StringUtils;

import com.example.demo.githubapi.GitHubApi;
import com.example.demo.githubapi.properties.Properties;
import com.example.demo.model.Repository;

@SpringBootApplication
public class GemographyBackendCodingChallengeApplication implements CommandLineRunner {
	
	@Autowired
	private GitHubApi gitHubApi;

	public static void main(String[] args) {
		SpringApplication.run(GemographyBackendCodingChallengeApplication.class, args);
	}

	@Override
	public void run(String... args) throws Exception {
		
		// get the 100 starred repositories from github
		List<Repository> starredRepos = gitHubApi.getStarredRepos(LocalDate.now().minusMonths(1), 100);
		
		// some repositories have null language property 
		Function<Repository, String> LanguageFunction = repo -> {
			if(StringUtils.hasLength(repo.getLanguage())) {
				return repo.getLanguage().toLowerCase();
			}
			return "no-language";
		};
		
		// grouping repositories by language
		Map<String, List<Repository>> starredReposByLanguage = starredRepos.stream().collect(Collectors.groupingBy(LanguageFunction));
		
		// write result in a file for more readability
		Path outFilePath = ResourceUtils.getFile("classpath:static/result.txt").toPath();
		Files.deleteIfExists(outFilePath);
		Files.writeString(outFilePath, "\n\t\t >> backend-coding-challenge << \n\n" ,  StandardOpenOption.CREATE_NEW);
		
		System.out.println("out file: " + outFilePath.toAbsolutePath().toString());
		
		starredReposByLanguage.forEach((lang, repos) -> {
			try {
				Files.writeString(outFilePath, 
						Properties.getGithubFormattedLine(lang, repos.size(), 
								repos.stream().map(repo -> repo.getOwner().getLogin()).collect(Collectors.joining(", "))), 
						StandardOpenOption.APPEND);
			} catch (IOException e) {
				e.printStackTrace();
			}
		});
		
		System.out.println("========= end writing =========");
		
	}

}
