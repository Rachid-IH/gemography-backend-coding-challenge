# Gemography backend-coding-challenge

##the project is a Spring Boot Project.

To test the application execute the application as a java application and display the content of the logged file or check out the endpoint http://localhost:8081/result.txt

### logged file

![alt text](docs/img/log.png "logged file")

### result endpoint

![alt text](docs/img/result.png "result endpoint")